#pragma once
#ifndef OSZTO_H
#define OSZTO_H
#include "lap.h"
#include <string>

class Oszto
{
protected:
	int kartyakszama;
	lap* kartya;
public:
	static int aktLap;
	Oszto();
	virtual void lapHuzas(std::string Nev, int ertek);
	virtual int getLapOsszeg() const;
	virtual int getKartyakSzama();
	virtual void print() const;
	void printEgy() const;
	virtual ~Oszto();
};

#endif