#include "GameList.h"



GameList::GameList()
{
	darab = 0;
	lista = NULL;
}

void GameList::hozzaAd(InterGame & x)
{
	InterGame** tmp;
	tmp = new InterGame*[darab + 1];
	for (int i = 0; i < darab; i++) {
		tmp[i] = lista[i];
	}
	tmp[darab] = &x;
	darab++;
	delete[] lista;
	lista = tmp;
}

void GameList::kiir()
{
	for (int i = 0; i < darab; i++) {
		lista[i]->printszabalyok();
	}
}



GameList::~GameList()
{
	delete[] lista;
}
