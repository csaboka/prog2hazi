#include "GameMaster.h"
#include "blackjack.h"
#include "FelkaruRablo.h"
#include "InterGame.h"
#include "GameList.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <string>
#include <iomanip>

GameMaster::GameMaster()
{
	for (double j = 0; j < 14; j++) {
		std::cout << "Copyright: Csaba Hazi";
		for (int i = 0; i < 12; i++) {
			std::cout << std::endl;
		}
		std::cout << "                          Loading                                 " << std::endl;
		std::cout << "                      [";
		
		for (int x = 0; x < j; x++) {
			std::cout << "=";
		}
		for (int z = 0; z < (13 - j); z++) {
			std::cout << " ";
		}
		std::cout << "]";
		std::cout << std::endl;
		double sz = j*((double)100 / (double)14);
		std::cout << "                           " << std::setprecision(3) << sz << "%" << std::endl;

		for (int i = 0; i < 10; i++) {
			std::cout << std::endl;
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(15));
		system("cls");
	}
}

void GameMaster::centerCasino() const 
{
	std::string a = "=== |$$ Hazi's Casino $$| ===";
	int l = (int)a.length();
	int pos = (int)((80 - l) / 2);
	for (int i = 0; i < pos; i++) {
		std::cout << " ";
	}

	 
	std::cout << a << std::endl;
}

void GameMaster::gameMenufelirat() {
	centerCasino();
	std::cout << "Jatek kivalasztasa:" << std::endl;
	std::cout << " 1 >> BlackJack" << std::endl;
	std::cout << " 2 >> Felkaru rablo" << std::endl;
	std::cout << "-1 >> Jatekszabalyok" << std::endl;
	std::cout << " 0 >> Vissza a fomenube" << std::endl;

}

void GameMaster::jatekstart(Base &Jatbaz) {

	system("cls");

	int menuparancs;
	int jelzo = 1;

	while (jelzo == 1)
	{
		gameMenufelirat();

		Oszto::aktLap = 0;

		std::cout << "Kerek egy szamot 0-2 kozotti szamot" << std::endl;
		std::cin >> menuparancs;

		while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 && menuparancs != -1) {
			system("cls");
			gameMenufelirat();
			std::cout << std::endl;
			std::cout << "\t" << menuparancs << "   Nem 0-2 kozotti szam" << std::endl;
			std::cout << std::endl;
			std::cout << "Kerek egy szamot 0-2-ig" << std::endl;
			std::cin >> menuparancs;
		}

		if (menuparancs == 1) {
			if (Jatbaz.getDarab() < 2) {
				std::cout << "Nincs eleg jatekos" << std::endl;
			}
			else {
				blackjack BlackJatek;
				BlackJatek.jatekStart(Jatbaz);
			}
		}

		if (menuparancs == 2) {
			FelkaruRablo felkaru;
			felkaru.jatekStart(Jatbaz);

		}

		if (menuparancs == -1) {
			system("cls");
			centerCasino();
			blackjack BlackJatek;
			FelkaruRablo felkaru;
			GameList list;
			list.hozzaAd(BlackJatek);
			list.hozzaAd(felkaru);
			list.kiir();
			system("pause");
		}


		if (menuparancs == 0) {
			jelzo = 0;
		}

		system("cls");
	}
}

GameMaster::~GameMaster()
{
}
