#include "blackjack.h"
#include "Jatekos.h"
#include "Oszto.h"
#include "lap.h"
#include "pakli.h"
#include "Base.h"
#include "Zene.h"
#include <iostream>	
#include <chrono>
#include <thread>
#include <string>
#include <ctime>
#include <conio.h>
#include <iomanip>
#include <random>
#include <SFML/Audio.hpp>

blackjack::blackjack()
{
	this->JatekSzabaly = " Minimum 2 jatekos es maximum 5 jatekos jatszhat.\n Eloszor tetet kell rakni es azutan oszt az oszto mindenkinek 2 lapot.\n Maganak is 2 lapot oszt de ebbol csak 1 lathato az elejen.\n A jatekos koreben a jatekos huzhat lapot vagy biztositast kothet.\n Biztositast csak akkor kothet ha az osztonak a lathato kartyaja Asz.\n Ha egy jatekos lapjainak az osszege nagyobb mint 21 akkor vesztett.\n Ha valaki nyer az annyit nyer amiennyit berakott.\n Ha valakinek blackjackje van az a m�sf�lszereset nyeri meg.\n Ha valaki biztositast kotott es az osztonak Blackjack-je lett akkor\n visszakapja a biztositas ertekenek a ketszereset.\n Ha a lapok osszege megegyezik az osztojaval akkor visszakapjuk a tet erteket.";
}

void blackjack::toltofelirat()
{
	system("cls");
	centerBlackjack();
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "                     BlackJack inditasa..." << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	system("cls");
}

unsigned blackjack::jatekosSzam(Base &Jatbaz) {

	unsigned darab = 0;

	for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
		if (Jatbaz.getJatekosok()[i].getPenz() >= 10) {
			darab++;
		}
	}

	if (darab < 2) {
		std::cout << "Nincs eleg jatekos, akinek eleg penze lenne jatszani." << std::endl;
		system("pause");
		return -1;
	}

	unsigned szam;
	std::cout << "Hany jatekos fog jatszani?" << std::endl;
	std::cout << "(2-5)" << std::endl;
	std::cin >> szam;
	while (szam < 2 || szam > 5) {
		system("cls");
		std::cout << "Hany jatekos fog jatszani?" << std::endl;
		std::cout << "(2-5)" << std::endl;
		std::cout << "Ervenytelen szam" << std::endl;
		std::cin >> szam;
	}
	if (szam > Jatbaz.getDarab()) {
		std::cout << "Nincs eleg jatekos az adatbazisban." << std::endl;
		system("pause");
		return -1;
	}
	return szam;
}

int blackjack::vaneEleg(Base &Jatbaz) {
	int darab = 0;

	for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
		if (Jatbaz.getJatekosok()[i].getPenz() >= 10) {
			darab++;
		}
	}

	if (darab < 2) {
		std::cout << "Nincs eleg jatekos, akinek eleg penze lenne jatszani." << std::endl;
		system("pause");
		return -1;
	}
	else {
		return 0;
	}
}

void blackjack::setJatszik(Base &Jatbaz, unsigned szam) {
	if (szam == Jatbaz.getDarab()) {
		for (unsigned i = 0; i < szam; i++) {
			Jatbaz.getJatekosok()[i].setJatszik(true);
		}
		return;
	}
	for(unsigned j = 0; j < szam; j++){
		system("cls");
		unsigned x;
		Jatbaz.print();
		std::cout << j+1 << ". jatekos szama" << std::endl;
		std::cin >> x;
		while (x < 1 || x > Jatbaz.getDarab() || (Jatbaz.getJatekosok()[x-1].getJatszik() == true)) {
			system("cls");
			Jatbaz.print();
			std::cout << j+1 << ". jatekos szama" << std::endl;
			std::cout << "Ervenytelen szam" << std::endl;
			std::cin >> x;
		}
		Jatbaz.getJatekosok()[x - 1].setJatszik(true);
	}
	return;
}

void blackjack::egyJatekosBe(Base &Jatbaz) {
	system("cls");
	if (jatekban(Jatbaz.getJatekosok(), Jatbaz) == Jatbaz.getDarab()) {
		centerBlackjack();
		std::cout << "Az osszes jatekos jatszik." << std::endl;
		system("pause");
		return;
	}
	if (jatekban(Jatbaz.getJatekosok(), Jatbaz) == 5) {
		centerBlackjack();
		std::cout << "Max 5 jatekos jatszhat." << std::endl;
		system("pause");
		return;
	}
	unsigned x;
	Jatbaz.print();
	std::cout << "Hanyas Jatekos szeretne leulni?" << std::endl;
	std::cout << "(0 << Vissza)" << std::endl;
	std::cin >> x;
	if (x == 0) {
		return;
	}
	while (/*x < 0 ||*/ x > Jatbaz.getDarab() || (Jatbaz.getJatekosok()[x - 1].getJatszik() == true)) {
		system("cls");
		Jatbaz.print();
		std::cout << "Hanyas Jatekos szeretne leulni?" << std::endl;
		std::cout << "(0 << Vissza)" << std::endl;
		std::cout << "Ervenytelen szam" << std::endl;
		std::cin >> x;
		if (x == 0) {
			return;
		}
	}
	Jatbaz.getJatekosok()[x - 1].setJatszik(true);
}

void blackjack::egyJatekosKi(Base &Jatbaz) {
	system("cls");
	if (jatekban(Jatbaz.getJatekosok(), Jatbaz) == 2) {
		centerBlackjack();
		std::cout << "Minimum 2 jatekosnak jatszani kell" << std::endl;
		system("pause");
		return;
	}
	unsigned x;
	Jatbaz.print();
	std::cout << "Hanyas Jatekos szeretne felallni?" << std::endl;
	std::cout << "(0 << Vissza)" << std::endl;
	std::cin >> x;
	if (x == 0) {
		return;
	}
	while (/*x < 0 ||*/ x > Jatbaz.getDarab() || (Jatbaz.getJatekosok()[x - 1].getJatszik() == false)) {
		system("cls");
		Jatbaz.print();
		std::cout << "Hanyas Jatekos szeretne felallni?" << std::endl;
		std::cout << "(0 << Vissza)" << std::endl;
		std::cout << "Ervenytelen szam" << std::endl;
		std::cin >> x;
		if (x == 0) {
			return;
		}
	}
	Jatbaz.getJatekosok()[x - 1].setJatszik(false);
}

void blackjack::centerBlackjack() const {
	std::string a = "=== |$$ BlackJack $$| ===";
	int l = (int)a.length();
	int pos = (int)((80 - l) / 2);
	for (int i = 0; i < pos; i++) {
		std::cout << " ";
	}


	std::cout << a << std::endl;
}

void blackjack::blackjackMenu() {
	system("cls");
	centerBlackjack();
	std::cout << std::endl;
	std::cout << " 1 >> Jatek" << std::endl;
	std::cout << " 2 >> Jatekos leultetese" << std::endl;
	std::cout << " 3 >> Jatekos felallitasa" << std::endl;
	std::cout << " 0 >> Vissza" << std::endl;
}

void blackjack::pakliKeveres(pakli &pak) {

	sf::SoundBuffer buffer;
	buffer.loadFromFile("Effects/BlackJack/PakliKever.wav");

	Oszto::aktLap = 0;

	unsigned seed = static_cast<int> (std::chrono::system_clock::now().time_since_epoch().count());
	std::mt19937 generator(seed);
	std::uniform_int_distribution<int> distribution(10000, 50000);
	int numPicked = distribution(generator);

	sf::Sound sound;
	sound.setBuffer(buffer);
	sound.setVolume(Zene::EffectHangEro);
	sound.play();

	pak.kever(numPicked);
}

void blackjack::TetRakas(Jatekos aktJat[], int darab) {
	double z;
	system("cls");
	centerBlackjack();
	for (int i = 0; i < darab; i++) {
		if (aktJat[i].getJatszik()) {
			std::cout << "Kerem " << aktJat[i].getNeve() << " jatekos tetjet." << std::endl;
			std::cout << "       (10-50)" << std::endl;
			std::cin >> z;
			std::cout << std::endl;
			while (z < 10 || z > 50) {
				system("cls");
				centerBlackjack();
				std::cout << "Ervenytelen tet rakas." << std::endl;
				std::cout << "Kerem " << aktJat[i].getNeve() << " jatekos tetjet." << std::endl;
				std::cout << "       (10-50)" << std::endl;
				std::cin >> z;
				std::cout << std::endl;
			}
			aktJat[i].tet(z);
		}
	}
}

int blackjack::jatekban(Jatekos aktJat[], Base &Jatbaz) {
	int jat = 0;
	for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
		if (aktJat[i].getJatszik()) {
			jat++;
		}
	}
	return jat;
}

void blackjack::eredmeny(Oszto &dealer, Jatekos Jat[], int darab) {
	for (int i = 0; i < darab; i++) {
		if (Jatekos::AszaVan == true && dealer.getLapOsszeg() == 21 && dealer.getKartyakSzama() == 2) {
			double v = Jat[i].getPenz() + 2 * Jat[i].getBiztositas();
			Jat[i].setPenz(v);
			Jat[i].setBiztositas(0.0);
		}
		if (Jat[i].getJatszik() == true && Jat[i].getBlackJack() == false && Jat[i].getLapOsszeg() <= 21) {
			if (Jat[i].getLapOsszeg() > dealer.getLapOsszeg() || dealer.getLapOsszeg() > 21) {
				double z = Jat[i].getPenz() + 2*Jat[i].getAktTet();
				Jat[i].setPenz(z);
			}
		}
		if (Jat[i].getJatszik() == true && Jat[i].getBlackJack() == false && Jat[i].getLapOsszeg() <= 21) {
			if (Jat[i].getLapOsszeg() == dealer.getLapOsszeg()) {
				double y = Jat[i].getPenz() + Jat[i].getAktTet();
				Jat[i].setPenz(y);
			}
		}
		if (Jat[i].getBlackJack() == true) {
			double x = Jat[i].getPenz() + ((double)5/(double)2) * Jat[i].getAktTet();
			Jat[i].setPenz(x);
			Jat[i].setBlackJack(false);
		}
	}

}

void blackjack::printszabalyok()
{
	std::cout << "Blackjack jatekszabalya:" << std::endl;
	std::cout << JatekSzabaly << std::endl;
	std::cout << std::endl;
}

void blackjack::jatekosKorFelirat(Jatekos &jat) {
	centerBlackjack();
	std::cout << std::endl;
	std::cout << std::endl;
	jat.print();
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << jat.getNeve() << " kore:" << std::endl;
	std::cout << "1 >> Lap huzas" << std::endl;
	if (Jatekos::AszaVan == true) {
		std::cout << "2 >> Biztositas kotese" << std::endl;
	}
	std::cout << "0 >> Megall" << std::endl;
}

void blackjack::lapOsztEffect() {
	sf::SoundBuffer buffer;
	buffer.loadFromFile("Effects/BlackJack/KartyaOszt.wav");

	sf::Sound sound;
	sound.setBuffer(buffer);
	sound.setVolume(Zene::EffectHangEro);
	sound.play();
	std::this_thread::sleep_for(std::chrono::milliseconds(400));
}

void blackjack::jatBiztositas(Jatekos &Jat) {
	system("cls");
	centerBlackjack();
	double bizt;
	std::cout << "Kerem a biztositas erteket!" << std::endl;
	std::cout << "Max a rakott tet fele lehet (" << Jat.getAktTet()/2 << ")" << std::endl;
	std::cin >> bizt;
	while (bizt < (double)0 && bizt > (Jat.getAktTet() / (double)2)) {
		std::cout << "Kerem a biztositas erteket!" << std::endl;
		std::cout << "Max a rakott tet fele lehet (" << Jat.getAktTet() / 2 << ")" << std::endl;
		std::cout << "  Ervenytelen ertek" << std::endl;
		std::cin >> bizt;
	}
	Jat.setBiztositas(bizt);
}

void blackjack::jatekosKor(Jatekos Jat[], pakli &pak, int darab) {
	int menuparancs;
	int jelzo = 1;

	for (int j = 0; j < darab; j++) {
		if (Jat[j].getJatszik()) {
			jelzo = 1;
			if (Jat[j].getLapOsszeg() == 21 && Jat[j].getKartyakSzama() == 2) {
				system("cls");
				centerBlackjack();

				sf::SoundBuffer buffer;
				buffer.loadFromFile("Effects/BlackJack/BlackJack.wav");

				sf::Sound sound;
				sound.setBuffer(buffer);
				sound.setVolume(Zene::EffectHangEro);
				sound.play();

				std::cout << std::endl;
				std::cout << "BLACKJACK" << std::endl;
				std::cout << Jat[j].getNeve() << std::endl;
				Jat[j].setBlackJack(true);
				system("pause");
				jelzo = 0;
			}
			while (jelzo == 1)
			{

				system("cls");
				jatekosKorFelirat(Jat[j]);

				if (Jatekos::AszaVan == true) {
					std::cout << "Kerek egy 0-2 kozotti szamot" << std::endl;
				}
				else {
					std::cout << "Kerek egy 0-1 kozotti szamot" << std::endl;
				}
				std::cin >> menuparancs;

				while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 /*&& menuparancs != 3*/) {
					system("cls");
					jatekosKorFelirat(Jat[j]);
					std::cout << std::endl;
					if (Jatekos::AszaVan == true) {
						std::cout << "\t" << menuparancs << "   Nem 0-2 kozotti szam" << std::endl;
					}
					else {
						std::cout << "\t" << menuparancs << "   Nem 0-1 kozotti szam" << std::endl;
					}
					std::cout << std::endl;
					if (Jatekos::AszaVan == true) {
						std::cout << "Kerek egy 0-2 kozotti szamot" << std::endl;
					}
					else {
						std::cout << "Kerek egy 0-1 kozotti szamot" << std::endl;
					}
					std::cin >> menuparancs;
				}

				if (menuparancs == 1) {
					lapOsztEffect();
					Jat[j].lapHuzas(pak.getCardType(Oszto::aktLap), pak.getValue(Oszto::aktLap));
					if (Jat[j].getLapOsszeg() > 21) {
						system("cls");
						centerBlackjack();
						Jat[j].print();
						sf::SoundBuffer buffer2;
						buffer2.loadFromFile("Effects/BlackJack/ULose.wav");

						sf::Sound sound2;
						sound2.setBuffer(buffer2);
						sound2.setVolume(Zene::EffectHangEro);
						sound2.play();
						jelzo = 0;
						std::this_thread::sleep_for(std::chrono::milliseconds(1500));
						system("pause");
					}
				}

				if (menuparancs == 2) {
					if (Jatekos::AszaVan == true) {
						jatBiztositas(Jat[j]);
					}
				}


				if (menuparancs == 0) {
					jelzo = 0;
				}

				system("cls");

			}
		}
	}
}

void blackjack::dealerKor(Oszto &dealer, Jatekos Jat[], pakli &pak, int darab) {
	while (dealer.getLapOsszeg() < 17) {
		lapOsztEffect();
		dealer.lapHuzas(pak.getCardType(Oszto::aktLap), pak.getValue(Oszto::aktLap));
		print(false, dealer, Jat, darab);
		std::this_thread::sleep_for(std::chrono::milliseconds(1001));
	}
}

void blackjack::print(bool osztoEgy, Oszto &dealer, Jatekos Jat[], int darab) {
	system("cls");
	centerBlackjack();

	std::cout << std::endl;
	std::cout << std::endl;

	if (osztoEgy) {
		dealer.printEgy();
		std::cout << std::endl;
	}
	else {
		dealer.print();
		std::cout << std::endl;
	}
	for (int i = 0; i < darab; i++) {
		if (Jat[i].getJatszik()) {
			Jat[i].print();
			std::cout << std::endl;
		}
	}
}

void blackjack::effectJatPrint(Jatekos &Jat) {
	system("cls");
	centerBlackjack();
	Jat.print();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

void blackjack::effectDealerPrint(Oszto &dealer) {
	system("cls");
	centerBlackjack();
	dealer.printEgy();
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

void blackjack::jatekSetup(Oszto &dealer, Jatekos Jat[], pakli &pak, int darab, Base &Jatbase) {
	system("cls");
	bool egymutat = true;
	centerBlackjack();
	TetRakas(Jat, darab);

	for (int x = 0; x < 2; x++) {
		for (int i = 0; i < darab; i++) {
			if (Jat[i].getJatszik()) {
				lapOsztEffect();
				Jat[i].lapHuzas(pak.getCardType(Oszto::aktLap), pak.getValue(Oszto::aktLap));
				effectJatPrint(Jat[i]);
				if (Jat[i].getLapOsszeg() == 21) {
					Jat[i].setBlackJack(true);
				}

			}
		}
		lapOsztEffect();
		dealer.lapHuzas(pak.getCardType(Oszto::aktLap), pak.getValue(Oszto::aktLap));
		if (x == 0) {
			if (dealer.getLapOsszeg() == 11) {
				Jatekos::AszaVan = true;
			}
			effectDealerPrint(dealer);
		}
	}

	std::cout << std::endl;

	print(egymutat, dealer, Jat, darab);

	system("pause");

	jatekosKor(Jat, pak, darab);

	print(egymutat, dealer, Jat, darab);
	
	egymutat = false;

	print(egymutat, dealer, Jat, darab);

	dealerKor(dealer, Jat, pak, darab);

	print(egymutat, dealer, Jat, darab);

	eredmeny(dealer, Jat, darab);

	Jatbase.FileWrite();

	Jatekos::AszaVan = false;
}

void blackjack::jatek(Base &Jatbaz, pakli &pak) {
	system("cls");
	centerBlackjack();

	for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
		if (Jatbaz.getJatekosok()[i].getJatszik()) {
			if (Jatbaz.getJatekosok()[i].getPenz() < 10) {
				std::cout << "Nincs eleg penze a " << Jatbaz.getJatekosok()[i].getNeve() <<  " jatekosnak." << std::endl;
				system("pause");
				return;
			}
		}
	}

	if (jatekban(Jatbaz.getJatekosok(), Jatbaz) < 2) {
		std::cout << "Nincs eleg jatekos" << std::endl;
		system("pause");
		return;
	}

	pakliKeveres(pak);

	jatekSetup(Jatbaz.getOszto(), Jatbaz.getJatekosok(), pak, Jatbaz.getDarab(), Jatbaz);

	system("pause");
}

void blackjack::jatekStart(Base &Jatbaz) {

	toltofelirat();

	system("cls");
	centerBlackjack();

	int x = jatekosSzam(Jatbaz);
	if (x == -1) {
		return;
	}


	setJatszik(Jatbaz, x);

	pakli pak;

	int menuparancs;
	int jelzo = 1;

	while (jelzo == 1)
	{


		blackjackMenu();

		std::cout << "Kerek egy 0-3 kozotti szamot" << std::endl;
		std::cin >> menuparancs;

		while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 && menuparancs != 3) {
			system("cls");
			blackjackMenu();
			std::cout << std::endl;
			std::cout << "\t" << menuparancs << "   Nem 0-3 kozotti szam" << std::endl;
			std::cout << std::endl;
			std::cout << "Kerek egy szamot 0-3-ig" << std::endl;
			std::cin >> menuparancs;
		}

		if (menuparancs == 1) {
			if (vaneEleg(Jatbaz) == -1) {
				jelzo = 0;
			}

			if (jelzo == 1)	{
				jatek(Jatbaz, pak);
				for (unsigned z = 0; z < Jatbaz.getDarab(); z++) {
					if (Jatbaz.getJatekosok()[z].getPenz() < 10) {
						Jatbaz.getJatekosok()[z].setJatszik(false);
					}
					Jatbaz.getJatekosok()[z].setBlackJack(false);
					Jatbaz.getJatekosok()[z].setKartyaNULL();
					Jatbaz.getJatekosok()[z].setKartyaszama(0);
					Jatbaz.getJatekosok()[z].setAktTet(0);
					Oszto::aktLap = 0;
				}
			}
		}

		if (menuparancs == 2) {
			if (vaneEleg(Jatbaz) == -1) {
				jelzo = 0;
			}
			if (jelzo == 1) {
				egyJatekosBe(Jatbaz);
			}
		}

		if (menuparancs == 3) {
			if (vaneEleg(Jatbaz) == -1) {
				jelzo = 0;
			}
			if (jelzo == 1) {
				egyJatekosKi(Jatbaz);
			}
		}

		if (menuparancs == 0) {
			jelzo = 0;
		}

		system("cls");

	}

	for (unsigned z = 0; z < Jatbaz.getDarab(); z++) {
		Jatbaz.getJatekosok()[z].setJatszik(false);
		Jatbaz.getJatekosok()[z].setKartyaNULL();
		Jatbaz.getJatekosok()[z].setKartyaszama(0);
		Jatbaz.getJatekosok()[z].setAktTet(0);
		Oszto::aktLap = 0;
	}

}

blackjack::~blackjack()
{
}
