#include "lap.h"
#include <iostream>


lap::lap() {
	this->nev = "Inicializalatlan";
	this->ertek = 0;
}

void lap::setNev(std::string x) {
	this->nev = x;
}

void lap::setValue(int value) {
	this->ertek = value;
}

void lap::setBoth(std::string x, int value) {
	this->nev = x;
	this->ertek = value;
}


int lap::getValue() {
	return this->ertek;
}

std::string lap::getNev() {
	return this->nev;
}

void lap::print() const {
	if (this->nev.length() > 7) {
		std::cout << this->nev << "\t" << this->ertek << std::endl;
	}
	else {
		std::cout << this->nev << "\t\t" << this->ertek << std::endl;
	}
}

lap::~lap()
{
}


