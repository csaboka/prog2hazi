#include "Base.h"
#include "Oszto.h"
#include "Jatekos.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <regex>

Base::Base()
{
	this->darab = 0;
	this->jat = NULL;
}

void Base::centerJatbase() {
	std::string s = "=== |$$ Jatekosbazis $$| ===";
	int l = (int)s.length();
	int pos = (int)((80 - l) / 2);
	for (int i = 0; i < pos; i++) {
		std::cout << " ";
	}

	std::cout << s << std::endl;
}

void Base::centerMenu()
{
	centerJatbase();

	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;

	std::cout << "1 >> Hozzaad" << std::endl;
	std::cout << "2 >> Torol" << std::endl;
	std::cout << "3 >> Kiiras" << std::endl;
	std::cout << "4 >> Mentes" << std::endl;
	std::cout << "0 >> Vissza a fomenube" << std::endl;

}

void Base::capt(std::string &nev) {
	nev[0] = toupper(nev[0]);
	int a = (int)nev.length();
	for (int i = 1; i < a; i++)
	{
		if (nev[i - 1] == ' ')
			nev[i] = toupper(nev[i]);
	}
}

Oszto Base::getOszto() {
	return this->dealer;
}

Jatekos* Base::getJatekosok() {
	return jat;
}

void Base::hozzaAddFilebol() {
	std::string input_str;
	std::ifstream myfile;
	myfile.open("Options\\Jatekosok.sav");
	if (myfile.is_open())
	{
		while (getline(myfile, input_str))
		{
			int pos = (int)input_str.find_first_of('\t');
			std::string penz = input_str.substr(pos + 1),
				neve = input_str.substr(0, pos);

			int penzInt = std::stoi(penz);
			hozzaAdd(neve, penzInt);
		}
		myfile.close();
	}
	else {
		throw std::runtime_error("Nem lehetett megnyitni a fajlt: Jatekosok.sav");
	}
}

void Base::hozzaAdd(std::string &z, int const &penz) {
	Jatekos* tmp;
	unsigned i = 0;
	for (unsigned j = 0; j < darab; j++) {
		if (z.compare(jat[j].getNeve()) == 0) {
			return;
		}
	}
	tmp = new Jatekos[darab + 1];
	if (jat != NULL) {
		for (i = 0; i < darab; i++) {
			tmp[i].setPenz(jat[i].getPenz());
			tmp[i].setSorszam(i + 1);
			tmp[i].setNeve(jat[i].getNeve());
		}
	}
	tmp[i].setNeve(z);
	std::cout << std::endl;
	tmp[i].setPenz(penz);
	tmp[i].setSorszam(i + 1);
	delete[] jat;
	darab++;
	jat = tmp;
}

void Base::hozzaAdd() {
	Jatekos* tmp;
	unsigned i = 0;
	std::string z;
	std::string semmi;
	std::cout << "Mi legyen a jatekos neve?" << std::endl;
	std::getline(std::cin, semmi);
	std::getline(std::cin, z);
	z = std::regex_replace(z, std::regex("^ +| +$|( ) +"), "$1");
	std::transform(z.begin(), z.end(), z.begin(), ::tolower);
	capt(z);
	for (unsigned j = 0; j < darab; j++) {
		if (z.compare(jat[j].getNeve()) == 0) {
			return;
		}
	}
	tmp = new Jatekos[darab + 1];
	if (jat != NULL) {
		for (i = 0; i < darab; i++) {
			tmp[i].setPenz(jat[i].getPenz());
			tmp[i].setSorszam(i+1);
			tmp[i].setNeve(jat[i].getNeve());
		}
	}
	tmp[i].setNeve(z);
	std::cout << std::endl;
	tmp[i].setSorszam(i + 1);
	delete[] jat;
	darab++;
	jat = tmp;
}

void Base::torol(int sorSzam) {
	unsigned i, j;
	if (darab == 0) {
		std::cout << "Nincs egy jatekos sem" << std::endl;
		system("pause");
		return;
	}
	Jatekos* tmp;
	tmp = new Jatekos[darab - 1];
	for (i = 0, j = 0; i < darab-1; i++, j++) {
		if (j == sorSzam - 1) { 
			j++;
		}
		tmp[i].setPenz(jat[j].getPenz());
		tmp[i].setSorszam(i + 1);
		tmp[i].setNeve(jat[j].getNeve());
	}
	delete[] jat;
	darab--;
	jat = tmp;
}

void Base::FileWrite() {
	std::ofstream myfile;
	myfile.open("Options\\Jatekosok.sav");
	for (unsigned i = 0; i < darab; i++) {
		myfile << jat[i].getNeve() << "\t" << jat[i].getPenz() << std::endl;
	}
	myfile.close();
}

void Base::print() {
	system("cls");
	centerJatbase();
	if (darab == 0) {
		std::cout << "Nincs jatekos" << std::endl;
		return;
	}
	for (unsigned i = 0; i < darab; i++) {
		jat[i].printAll();
	}
}

unsigned Base::getDarab() {
	return this->darab;
}

void Base::menu() {

	system("cls");

	int menuparancs;
	int jelzo = 1;

	while (jelzo == 1)
	{


		centerMenu();

		std::cout << "Kerek egy 0-4 kozotti szamot" << std::endl;
		std::cin >> menuparancs;

		while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 && menuparancs != 3 && menuparancs != 4) {
			system("cls");
			centerMenu();
			std::cout << std::endl;
			std::cout << "\t" << menuparancs << "   Nem 0-4 kozotti szam" << std::endl;
			std::cout << std::endl;
			std::cout << "Kerek egy szamot 0-4-ig" << std::endl;
			std::cin >> menuparancs;
		}

		if (menuparancs == 1) {
			system("cls");
			centerJatbase();
			hozzaAdd();
			FileWrite();
		}

		if (menuparancs == 2) {
			print();
			unsigned x;
			if (darab != 0) {
				std::cout << "Hanyas jatekost szeretne torolni?" << std::endl;
				std::cin >> x;
				while (x < 1 || x > darab) {
					print();
					std::cout << "Hanyas jatekost szeretne torolni?" << std::endl;
					std::cout << "Nincs ilyen szamu jatekos" << std::endl;
					std::cin >> x;
				}
				torol(x);
				FileWrite();
			}
			else {
				system("pause");
			}
		}

		if (menuparancs == 3) {
			print();
			system("pause");
		}

		if (menuparancs == 4) {
			FileWrite();
		}


		if (menuparancs == 0) {
			jelzo = 0;
		}

		system("cls");

	}
}

Base::~Base()
{
	FileWrite();
	delete[] jat;
}
