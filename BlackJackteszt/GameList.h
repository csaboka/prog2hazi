#pragma once
#ifndef GAMELIST_H
#define GAMELIST_H

#include "InterGame.h"


class GameList
{
	int darab;
	InterGame** lista;
public:
	GameList();
	void hozzaAd(InterGame &x);
	void kiir();
	~GameList();
};

#endif