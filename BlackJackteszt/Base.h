#pragma once
#ifndef BASE_H
#define BASE_H

#include "Jatekos.h"
#include "Oszto.h"
#include <string>

class Base
{
	Oszto dealer;
	Jatekos* jat;
	unsigned darab;
public:
	Base();
	void menu();
	void centerJatbase();
	void centerMenu();
	Oszto getOszto();
	Jatekos* getJatekosok();
	void capt(std::string &nev);
	//std::string remove_spaces(const std::string &s);
	void hozzaAddFilebol();
	void hozzaAdd(std::string &z, int const &penz);
	void FileWrite();
	void hozzaAdd();
	unsigned getDarab();
	void torol(int sorSzam);
	void print();
	~Base();
};

#endif