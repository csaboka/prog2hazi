#include "FelkaruRablo.h"
#include "Base.h"
#include "Zene.h"
#include <ctime>
#include <thread>
#include <iostream>
#include <string>
#include <ctime>
#include <conio.h>
#include <iomanip>
#include <random>
#include <SFML/Audio.hpp>

FelkaruRablo::FelkaruRablo()
{
	this->JatekSzabaly = "Tet kell rakni es ha mar 2 megegyezo szam van akkor nyertel.";
}

void FelkaruRablo::toltoFelirat()
{
	system("cls");
	centerFelkaru();
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "                     Felkaru Rablo inditasa..." << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	system("cls");
}

void FelkaruRablo::centerFelkaru() const {
	std::string a = "=== |$$ Felkaru Rablo $$| ===";
	int l = (int)a.length();
	int pos = (int)((75 - l) / 2);
	for (int i = 0; i < pos; i++) {
		std::cout << " ";
	}


	std::cout << a << std::endl;
}

void FelkaruRablo::menufelirat(int const &a, int const &b, int const &c) {
	centerFelkaru();
	print(a, b, c);
	std::cout << " 1 >> Start" << std::endl;
	std::cout << " 2 >> Jatekos valtas" << std::endl;
	std::cout << " 0 >> Vissza a fomenube" << std::endl;
};

void FelkaruRablo::print(int const &a, int const &b, int const &c) {
	system("cls");
	centerFelkaru();
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "\t\t" << "==========================================" << std::endl;
	std::cout << "\t\t" << "=             =             =            =" << std::endl;
	std::cout << "\t\t" << "=             =             =            =" << std::endl;
	std::cout << "\t\t" << "=      " << a << "      =      " << b << "      =      " << c << "     =" << std::endl;
	std::cout << "\t\t" << "=             =             =            =" << std::endl;
	std::cout << "\t\t" << "=             =             =            =" << std::endl;
	std::cout << "\t\t" << "==========================================" << std::endl;
	std::cout << "\t\t" << "=                                        =" << std::endl;
	std::cout << "\t\t" << "=                                        =" << std::endl;
	std::cout << "\t\t" << "=              Hazi's Casino             =" << std::endl;
	std::cout << "\t\t" << "=                                        =" << std::endl;
	std::cout << "\t\t" << "=                                        =" << std::endl;
	std::cout << "\t\t" << "=                                        =" << std::endl;
}

void FelkaruRablo::printKamu(int const &x, int const &y) {
	system("cls");
	typedef std::chrono::system_clock Clock;

	int k1 = 5;
	int k2 = 9;
	int v = 13;
	
	for (int i = 0; i < v; i++) {
		centerFelkaru();
		auto now = Clock::now();
		auto seconds = std::chrono::time_point_cast<std::chrono::seconds>(now);
		auto fraction = now - seconds;
		time_t cnow = Clock::to_time_t(now);

		auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(fraction);

		int a = milliseconds.count() / 100 % 10;
		int b = milliseconds.count() / 10 % 10;
		int c = milliseconds.count() % 10;
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << "\t\t" << "==========================================" << std::endl;
		std::cout << "\t\t" << "=             =             =            =" << std::endl;
		std::cout << "\t\t" << "=             =             =            =" << std::endl;
		if (i < k1) {
			std::cout << "\t\t" << "=      " << a << "      =      " << b << "      =      " << c << "     =" << std::endl;
		}
		if (k1 <= i && i < k2) {
			std::cout << "\t\t" << "=      " << x << "      =      " << b << "      =      " << c << "     =" << std::endl;
		}
		if (k2 <= i && i < v) {
			std::cout << "\t\t" << "=      " << x << "      =      " << y << "      =      " << c << "     =" << std::endl;
		}
		std::cout << "\t\t" << "=             =             =            =" << std::endl;
		std::cout << "\t\t" << "=             =             =            =" << std::endl;
		std::cout << "\t\t" << "==========================================" << std::endl;
		std::cout << "\t\t" << "=                                        =" << std::endl;
		std::cout << "\t\t" << "=                                        =" << std::endl;
		std::cout << "\t\t" << "=              Hazi's Casino             =" << std::endl;
		std::cout << "\t\t" << "=                                        =" << std::endl;
		std::cout << "\t\t" << "=                                        =" << std::endl;
		std::cout << "\t\t" << "=                                        =" << std::endl;

		srand((unsigned)time(NULL));
		std::this_thread::sleep_for(std::chrono::milliseconds(rand()% 100 + 500));

		system("cls");
	}
}

void FelkaruRablo::Tetrakas(Base &Jatbaz) {
	sf::SoundBuffer buffer;
	buffer.loadFromFile("Effects/Felkaru/Tetrakas.wav");

	sf::SoundBuffer buffer2;
	buffer2.loadFromFile("Effects/Felkaru/Start.wav");

	system("cls");
	centerFelkaru();
	double z;
	for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
		if (Jatbaz.getJatekosok()[i].getJatszik()) {
			std::cout << "Kerem " << Jatbaz.getJatekosok()[i].getNeve() << " jatekos tetjet." << std::endl;
			std::cout << "(0 is lehet)" << std::endl;
			std::cin >> z;
			while (z > Jatbaz.getJatekosok()[i].getPenz()) {
				system("cls");
				centerFelkaru();
				std::cout << "Ervenytelen tet rakas." << std::endl;
				std::cout << "Kerem " << Jatbaz.getJatekosok()[i].getNeve() << " jatekos tetjet." << std::endl;
				std::cout << "(0 is lehet)" << std::endl;
				std::cin >> z;
			}

			Jatbaz.FileWrite();

			sf::Sound sound;
			sound.setBuffer(buffer);
			sound.setVolume(Zene::EffectHangEro);
			sound.play();

			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			sf::Sound sound2;
			sound2.setBuffer(buffer2);
			sound2.setVolume(Zene::EffectHangEro);
			sound2.play();

			std::this_thread::sleep_for(std::chrono::milliseconds(750));

			Jatbaz.getJatekosok()[i].tet(z);
		}
	}
}

void FelkaruRablo::gyozelem(int const &a, int const &b, int const &c, Base &Jatbaz) {
	sf::SoundBuffer buffer;
	buffer.loadFromFile("Effects/Felkaru/Jackpot.wav");

	double z = 0;
	double x = 0;
	if ((a == 0 && b == 0) || (a == 0 && c == 0) || (b == 0 && c == 0)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 10 + Jatbaz.getJatekosok()[i].getPenz() + Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 0 && b == 0 && c == 0) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 100 + Jatbaz.getJatekosok()[i].getPenz() + 3 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 1 && b == 1) || (a == 1 && c == 1) || (b == 1 && c == 1)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 20 + Jatbaz.getJatekosok()[i].getPenz() + 1.2 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 1 && b == 1 && c == 1) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 120 + Jatbaz.getJatekosok()[i].getPenz() + 3.2 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 2 && b == 2) || (a == 2 && c == 2) || (b == 2 && c == 2)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 30 + Jatbaz.getJatekosok()[i].getPenz() + 1.4 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}

	if (a == 2 && b == 2 && c == 2) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 140 + Jatbaz.getJatekosok()[i].getPenz() + 3.4 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 3 && b == 3) || (a == 3 && c == 3) || (b == 3 && c == 3)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 40 + Jatbaz.getJatekosok()[i].getPenz() + 1.6 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 3 && b == 3 && c == 3) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 160 + Jatbaz.getJatekosok()[i].getPenz() + 3.6 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 4 && b == 4) || (a == 4 && c == 4) || (b == 4 && c == 4)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 50 + Jatbaz.getJatekosok()[i].getPenz() + 1.8 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 4 && b == 4 && c == 4) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 180 + Jatbaz.getJatekosok()[i].getPenz() + 3.8 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 5 && b == 5) || (a == 5 && c == 5) || (b == 5 && c == 5)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 60 + Jatbaz.getJatekosok()[i].getPenz() + 2 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 5 && b == 5 && c == 5) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 200 + Jatbaz.getJatekosok()[i].getPenz() + 4 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 6 && b == 6) || (a == 6 && c == 6) || (b == 6 && c == 6)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 70 + Jatbaz.getJatekosok()[i].getPenz() + 2.2 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}

	if (a == 6 && b == 6 && c == 6) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 220 + Jatbaz.getJatekosok()[i].getPenz() + 4.2 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 7 && b == 7) || (a == 7 && c == 7) || (b == 7 && c == 7)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 80 + Jatbaz.getJatekosok()[i].getPenz() + 2.4 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}

	if (a == 7 && b == 7 && c == 7) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 240 + Jatbaz.getJatekosok()[i].getPenz() + 4.4 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 8 && b == 8) || (a == 8 && c == 8) || (b == 8 && c == 8)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 90 + Jatbaz.getJatekosok()[i].getPenz() + 2.6 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	
	if (a == 8 && b == 8 && c == 8) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 260 + Jatbaz.getJatekosok()[i].getPenz() + 4.6 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}
	if ((a == 9 && b == 9) || (a == 9 && c == 9) || (b == 9 && c == 9)) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 100 + Jatbaz.getJatekosok()[i].getPenz() + 3 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
			}
		}
	}

	if (a == 9 && b == 9 && c == 9) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				z = 300 + Jatbaz.getJatekosok()[i].getPenz() + 5 * Jatbaz.getJatekosok()[i].getAktTet();
				x = z - Jatbaz.getJatekosok()[i].getPenz();
				Jatbaz.getJatekosok()[i].setPenz(z);
				sf::Sound sound2;
				sound2.setBuffer(buffer);
				sound2.setVolume(Zene::EffectHangEro);
				sound2.play();
			}
		}
	}

	std::cout << "A nyeremeny: " << x << std::endl;
	if (x > 0) {
		for (unsigned i = 0; i < Jatbaz.getDarab(); i++) {
			if (Jatbaz.getJatekosok()[i].getJatszik()) {
				std::cout << Jatbaz.getJatekosok()[i].getNeve() << " penze: " << z << std::endl;
			}
		}
	}
	Jatbaz.FileWrite();
	system("pause");
	
}

void FelkaruRablo::forog(int &a, int &b, int &c, Base &Jatbaz) {

	Tetrakas(Jatbaz);

	typedef std::chrono::system_clock Clock;

	auto now = Clock::now();
	auto seconds = std::chrono::time_point_cast<std::chrono::seconds>(now);
	auto fraction = now - seconds;
	time_t cnow = Clock::to_time_t(now);


	auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(fraction);

	a = milliseconds.count() / 100 % 10;
	b = milliseconds.count() / 10 % 10;
	c = milliseconds.count() % 10;


	//std::cout << a << "  " << b << "  " << c << "  " << std::endl;


	sf::SoundBuffer buffer;
	buffer.loadFromFile("Effects/Felkaru/Forgas.wav");

	sf::Sound sound;
	sound.setBuffer(buffer);
	sound.setVolume(Zene::EffectHangEro);
	sound.play();

	printKamu(a, b);
	print(a, b, c);
	gyozelem(a, b, c, Jatbaz);
}

void FelkaruRablo::jatVal(Base &Jatbaz) {
	system("cls");
	unsigned x;
	Jatbaz.print();
	std::cout << "Jatekos szama" << std::endl;
	std::cin >> x;
	while (x < 1 || x > Jatbaz.getDarab() || (Jatbaz.getJatekosok()[x - 1].getJatszik() == true)) {
		system("cls");
		Jatbaz.print();
		std::cout << "Jatekos szama" << std::endl;
		std::cout << "Ervenytelen szam" << std::endl;
		std::cin >> x;
	}
	Jatbaz.getJatekosok()[x - 1].setJatszik(true);
}

void FelkaruRablo::valtas(Base &Jatbaz) {
	for (unsigned z = 0; z < Jatbaz.getDarab(); z++) {
		Jatbaz.getJatekosok()[z].setJatszik(false);
		Jatbaz.getJatekosok()[z].setAktTet(0);
	}
	jatVal(Jatbaz);
}

void FelkaruRablo::printszabalyok()
{
	std::cout << "Felkarurablo jatekszabalya:" << std::endl;
	std::cout << JatekSzabaly << std::endl;
	std::cout << std::endl;
}

void FelkaruRablo::jatekStart(Base &Jatbaz) {

	toltoFelirat();

	if (Jatbaz.getDarab() == 0) {
		std::cout << "Nincs jatekos a rendszerben" << std::endl;
		system("pause");
		return;
	}

	jatVal(Jatbaz);

	int menuparancs;
	int jelzo = 1;

	int a = 0;
	int b = 0;
	int c = 0;

	while (jelzo == 1)
	{
		menufelirat(a,b,c);



		std::cout << "Kerek egy 0-2 kozotti szamot" << std::endl;
		std::cin >> menuparancs;

		while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2) {
			system("cls");
			menufelirat(a,b,c);
			std::cout << std::endl;
			std::cout << "\t" << menuparancs << "   Nem 0-2 kozotti szam" << std::endl;
			std::cout << std::endl;
			std::cout << "Kerek egy szamot 0-2-ig" << std::endl;
			std::cin >> menuparancs;
		}

		if (menuparancs == 1) {
			forog(a,b,c, Jatbaz);
			for (unsigned z = 0; z < Jatbaz.getDarab(); z++) {
				Jatbaz.getJatekosok()[z].setAktTet(0);
			}
		}

		if (menuparancs == 2) {
			valtas(Jatbaz);
		}

		if (menuparancs == 0) {
			for (unsigned z = 0; z < Jatbaz.getDarab(); z++) {
				Jatbaz.getJatekosok()[z].setJatszik(false);
				Jatbaz.getJatekosok()[z].setAktTet(0);
			}
			jelzo = 0;
		}

		system("cls");

	}


}

FelkaruRablo::~FelkaruRablo()
{
}
