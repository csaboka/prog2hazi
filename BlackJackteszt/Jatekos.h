#pragma once
#ifndef JATEKOS_H
#define JATEKOS_H
#include <string>
#include "Oszto.h"

class Jatekos:public Oszto
{
	std::string neve;
	int sorszam;
	double penz;
	double biztositas;
	bool jatszik;
	bool BlackJack;
	double aktTet;
public:
	static bool AszaVan;
	Jatekos();
	void setJatszik(bool x);
	void printAll() const;
	void print() const;
	void setNeve(std::string neve);
	void setSorszam(int a);
	void setKartyaszama(int x);
	void setKartyaNULL();
	void setPenz(double value);
	void setBlackJack(bool z);
	void setBiztositas(double x);
	std::string getNeve();
	double getAktTet();
	double getBiztositas();
	int getLapOsszeg() const;
	int getKartyakSzama();
	bool getJatszik();
	bool getBlackJack();
	void lapHuzas(std::string Nev, int ertek);
	void tet(double a);
	void setAktTet(double a);
	double getPenz();
	~Jatekos();
};

#endif