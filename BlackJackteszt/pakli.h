#pragma once
#ifndef PAKLI_H
#define PAKLI_H
#include "lap.h"
#include <string>

class pakli
{
	lap FrancPakli[52];

public:
	pakli();
	void print();
	void kever(double a);
	void KevTolt(int a);
	std::string getCardType(int aktLap);
	int getValue(int aktLap);
	~pakli();
};

#endif