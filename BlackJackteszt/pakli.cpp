#include "pakli.h"
#include <stdlib.h>
#include <iostream>
#include <random>
#include <time.h>
#include <chrono>
#include <thread>
enum {jumbo = 10, dama = 10, kiraly = 10, asz = 11};

#define spade 06                 
#define club 05                  
#define diamond 04              
#define heart 03

pakli::pakli()
{
	FrancPakli[0].setBoth("Pikk\06 2", 2);
	FrancPakli[1].setBoth("Pikk\06 3", 3);
	FrancPakli[2].setBoth("Pikk\06 4", 4);
	FrancPakli[3].setBoth("Pikk\06 5", 5);
	FrancPakli[4].setBoth("Pikk\06 6", 6);
	FrancPakli[5].setBoth("Pikk\06 7", 7);
	FrancPakli[6].setBoth("Pikk\06 8", 8);
	FrancPakli[7].setBoth("Pikk\06 9", 9);
	FrancPakli[8].setBoth("Pikk\06 10", 10);
	FrancPakli[9].setBoth("Pikk\06 Jumbo", jumbo);
	FrancPakli[10].setBoth("Pikk\06 Dama", dama);
	FrancPakli[11].setBoth("Pikk\06 Kiraly", kiraly);
	FrancPakli[12].setBoth("Pikk\06 Asz", asz);
	FrancPakli[13].setBoth("Karo\04 2", 2);
	FrancPakli[14].setBoth("Karo\04 3", 3);
	FrancPakli[15].setBoth("Karo\04 4", 4);
	FrancPakli[16].setBoth("Karo\04 5", 5);
	FrancPakli[17].setBoth("Karo\04 6", 6);
	FrancPakli[18].setBoth("Karo\04 7", 7);
	FrancPakli[19].setBoth("Karo\04 8", 8);
	FrancPakli[20].setBoth("Karo\04 9", 9);
	FrancPakli[21].setBoth("Karo\04 10", 10);
	FrancPakli[22].setBoth("Karo\04 Jumbo", jumbo);
	FrancPakli[23].setBoth("Karo\04 Dama", dama);
	FrancPakli[24].setBoth("Karo\04 Kiraly", kiraly);
	FrancPakli[25].setBoth("Karo\04 Asz", asz);
	FrancPakli[26].setBoth("Treff\05 2", 2);
	FrancPakli[27].setBoth("Treff\05 3", 3);
	FrancPakli[28].setBoth("Treff\05 4", 4);
	FrancPakli[29].setBoth("Treff\05 5", 5);
	FrancPakli[30].setBoth("Treff\05 6", 6);
	FrancPakli[31].setBoth("Treff\05 7", 7);
	FrancPakli[32].setBoth("Treff\05 8", 8);
	FrancPakli[33].setBoth("Treff\05 9", 9);
	FrancPakli[34].setBoth("Treff\05 10", 10);
	FrancPakli[35].setBoth("Treff\05 Jumbo", jumbo);
	FrancPakli[36].setBoth("Treff\05 Dama", dama);
	FrancPakli[37].setBoth("Treff\05 Kiraly", kiraly);
	FrancPakli[38].setBoth("Treff\05 Asz", asz);
	FrancPakli[39].setBoth("Kor\03 2", 2);
	FrancPakli[40].setBoth("Kor\03 3", 3);
	FrancPakli[41].setBoth("Kor\03 4", 4);
	FrancPakli[42].setBoth("Kor\03 5", 5);
	FrancPakli[43].setBoth("Kor\03 6", 6);
	FrancPakli[44].setBoth("Kor\03 7", 7);
	FrancPakli[45].setBoth("Kor\03 8", 8);
	FrancPakli[46].setBoth("Kor\03 9", 9);
	FrancPakli[47].setBoth("Kor\03 10", 10);
	FrancPakli[48].setBoth("Kor\03 Jumbo", jumbo);
	FrancPakli[49].setBoth("Kor\03 Dama", dama);
	FrancPakli[50].setBoth("Kor\03 Kiraly", kiraly);
	FrancPakli[51].setBoth("Kor\03 Asz", asz);
}

void pakli::print() {
	for (int i = 0; i <= 51; i++) {
		int value = FrancPakli[i].getValue();
		std::string nev = FrancPakli[i].getNev();
		if (nev.length() > 7) {
			std::cout << nev << "\t" << value << std::endl;
		}
		else {
			std::cout << nev << "\t\t" << value << std::endl;
		}
	}
}

void pakli::KevTolt(int a) {
	system("cls");
	for (int i = 0; i < 3; i++) {
		std::cout << std::endl;
	}
	std::cout << "                         Pakli keverese...                                " << std::endl;
	std::cout << "                          [";

	for (int x = 0; x < a; x++) {
		std::cout << "=";
		if (a % 2 == 0) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
	for (int z = 0; z < (10 - a); z++) {
		std::cout << " ";
	}
	std::cout << "]";
	std::cout << std::endl;
	std::cout << std::endl;
}

void pakli::kever(double a) {
	lap temp;
	int hiv = 0;
	std::cout << "Pakli keverese..." << std::endl;
	for (int i = 0; i <= a; i++) {
		int rnd1 = rand() % 52;
		int rnd2 = rand() % 52;
		if (i != 0) {
			if (i % ((int)a / 10) == 0) {
				hiv++;
				KevTolt(hiv);
			}
		}
		temp.setBoth(FrancPakli[rnd1].getNev(), FrancPakli[rnd1].getValue());
		FrancPakli[rnd1].setBoth(FrancPakli[rnd2].getNev(), FrancPakli[rnd2].getValue());
		FrancPakli[rnd2].setBoth(temp.getNev(), temp.getValue());
	}
	std::cout << std::endl;
	std::cout << "Pakli megkeverve." << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(458));
	system("cls");
	//std::cout << a << std::endl;
}

int pakli::getValue(int aktLap) {
	return this->FrancPakli[aktLap].getValue();
}

std::string pakli::getCardType(int aktLap) {
	return this->FrancPakli[aktLap].getNev();
}

pakli::~pakli()
{
}
