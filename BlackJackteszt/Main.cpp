﻿#include <iostream>
#include "Oszto.h"
#include "Jatekos.h"
#include "GameMaster.h"
#include "Base.h"
#include "Zene.h"
#include <string>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <conio.h>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

#define spade 06                 
#define club 05                  
#define diamond 04              
#define heart 03

void centerBlackjack()
{
	std::string s = "=== |$$ BlackJack $$| ===";
	int l = (int)s.length();
	int pos = (int)((80 - l) / 2);
	for (int i = 0; i < pos; i++) {
		std::cout << " ";
	}

	
	std::cout << s << std::endl;
}

void menuCredits() {
	system("cls");
	centerBlackjack();
	std::cout << "Copyright (C) 2017 Csaba Hazi" << std::endl;
	std::cout << "Credits to Csaba Hazi" << std::endl;
	std::cout << "Made for Prog2" << std::endl;
	std::cout << "v1.0" << std::endl;
	system("pause");
}

void fomenufelirat() {
	printf("\n%c%c%c%c%c     %c%c            %c%c         %c%c%c%c%c    %c    %c                ", club, club, club, club, club, spade, spade, diamond, diamond, heart, heart, heart, heart, heart, club, club);
	printf("\n%c    %c    %c%c           %c  %c       %c     %c   %c   %c              ", club, club, spade, spade, diamond, diamond, heart, heart, club, club);
	printf("\n%c    %c    %c%c          %c    %c     %c          %c  %c               ", club, club, spade, spade, diamond, diamond, heart, club, club);
	printf("\n%c%c%c%c%c     %c%c          %c %c%c %c     %c          %c %c              ", club, club, club, club, club, spade, spade, diamond, diamond, diamond, diamond, heart, club, club);
	printf("\n%c    %c    %c%c         %c %c%c%c%c %c    %c          %c%c %c             ", club, club, spade, spade, diamond, diamond, diamond, diamond, diamond, diamond, heart, club, club, club);
	printf("\n%c     %c   %c%c         %c      %c    %c          %c   %c               ", club, club, spade, spade, diamond, diamond, heart, club, club);
	printf("\n%c     %c   %c%c        %c        %c    %c     %c   %c    %c             ", club, club, spade, spade, diamond, diamond, heart, heart, club, club);
	printf("\n%c%c%c%c%c%c    %c%c%c%c%c%c%c   %c        %c     %c%c%c%c%c    %c     %c            ", club, club, club, club, club, club, spade, spade, spade, spade, spade, spade, spade, diamond, diamond, heart, heart, heart, heart, heart, club, club);
	printf("\n");
	printf("\n");
	printf("\n     %c%c%c%c%c%c%c%c      %c%c         %c%c%c%c%c    %c    %c                ", diamond, diamond, diamond, diamond, diamond, diamond, diamond, diamond, heart, heart, club, club, club, club, club, spade, spade);
	printf("\n        %c%c        %c  %c       %c     %c   %c   %c              ", diamond, diamond, heart, heart, club, club, spade, spade);
	printf("\n        %c%c       %c    %c     %c          %c  %c               ", diamond, diamond, heart, heart, club, spade, spade);
	printf("\n        %c%c       %c %c%c %c     %c          %c %c              ", diamond, diamond, heart, heart, heart, heart, club, spade, spade);
	printf("\n        %c%c      %c %c%c%c%c %c    %c          %c%c %c             ", diamond, diamond, heart, heart, heart, heart, heart, heart, club, spade, spade, spade);
	printf("\n        %c%c      %c      %c    %c          %c   %c               ", diamond, diamond, heart, heart, club, spade, spade);
	printf("\n     %c  %c%c     %c        %c    %c     %c   %c    %c             ", diamond, diamond, diamond, heart, heart, club, spade, spade, spade);
	printf("\n      %c%c%c      %c        %c     %c%c%c%c%c    %c     %c            ", diamond, diamond, diamond, heart, heart, club, club, club, club, club, spade, spade);
	printf("\n");
	printf("\n");
	printf("\n");
	std::cout << " 1 >> Start" << std::endl;
	std::cout << " 2 >> Jatekosok" << std::endl;
	std::cout << " 3 >> Zene" << std::endl;
	std::cout << " 4 >> Credits" << std::endl;
	std::cout << " 0 >> Kilepes" << std::endl;
}

int Oszto::aktLap = 0;
bool Jatekos::AszaVan = false;
float Zene::EffectHangEro = 0;
int Zene::ZeneHangEro = 0;

int main() {

	Zene z;
	sf::Music music;
	try {
		z.zeneFilebol();
	}
	catch (std::runtime_error &zene) {
		system("cls");
		std::cerr << "runtime_error: " << zene.what() << std::endl;
		std::cerr << "Inditsa ujra a programot" << std::endl;
		return 0;
	}
	z.play(music);

	GameMaster jatekmaster;
	Base jatekosBaz;
	try {
		jatekosBaz.hozzaAddFilebol();
	}
	catch (std::runtime_error &jatekosok) {
		system("cls");
		std::cerr << "runtime_error: " << jatekosok.what() << std::endl;
		std::cerr << "Inditsa ujra a programot" << std::endl;
		return 0;
	}

	int menuparancs;
	int jelzo = 1;
	int gomb = 0;

	while (jelzo == 1)
	{

		fomenufelirat();

		std::cout << "Kerek egy 0-4 kozotti szamot" << std::endl;

		std::cin >> menuparancs;
	

		while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 && menuparancs != 3 && menuparancs != 4) {
			system("cls");
			fomenufelirat();
			std::cout << std::endl;
			std::cout << "\t" << menuparancs << "   Nem 0-4 kozotti szam" << std::endl;
			std::cout << std::endl;
			std::cout << "Kerek egy szamot 0-4-ig" << std::endl;
			std::cin >> menuparancs;
		}

		if (menuparancs == 1) {
			jatekmaster.jatekstart(jatekosBaz);
		}

		if (menuparancs == 2) {
			jatekosBaz.menu();
		}

		if (menuparancs == 3) {
			z.zeneMenu(music);
		}

		if (menuparancs == 4) {
			menuCredits();
		}

		if (menuparancs == 0) {
			jelzo = 0;
		}

		system("cls");

	}

	return 0;
}