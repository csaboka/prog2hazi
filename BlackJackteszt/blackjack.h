#pragma once
#ifndef BLACKJACK_H
#define BLACKJACK_H
#include "Jatekos.h"
#include "Oszto.h"
#include "pakli.h"
#include "Base.h"
#include "InterGame.h"

class blackjack:public InterGame
{
public:
	blackjack();
	void toltofelirat();
	void centerBlackjack() const;
	void blackjackMenu();
	void pakliKeveres(pakli &pak);
	void jatekStart(Base &Jatbaz);
	void jatek(Base &Jatbaz, pakli &pak);
	void setJatszik(Base &Jatbaz, unsigned szam);
	void egyJatekosBe(Base &Jatbaz);
	void egyJatekosKi(Base &Jatbaz);
	void TetRakas(Jatekos aktJat[], int darab);
	void print(bool osztoEgy, Oszto &dealer, Jatekos Jat[], int darab);
	void jatekSetup(Oszto &dealer, Jatekos Jat[], pakli &pak, int darab, Base &Jatbase);
	void jatekosKor(Jatekos Jat[], pakli &pak, int darab);
	void dealerKor(Oszto &dealer, Jatekos Jat[], pakli &pak, int darab);
	void lapOsztEffect();
	void effectJatPrint(Jatekos &Jat);
	void effectDealerPrint(Oszto &dealer);
	void jatekosKorFelirat(Jatekos &jat);
	void jatBiztositas(Jatekos &Jat);
	void eredmeny(Oszto &dealer, Jatekos Jat[], int darab);
	void printszabalyok();
	int jatekban(Jatekos aktJat[], Base &Jatbaz);
	int vaneEleg(Base &Jatbaz);
	unsigned jatekosSzam(Base &Jatbaz);
	~blackjack();
};

#endif