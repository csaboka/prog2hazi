#pragma once
#ifndef FELKARURABLO_H
#define FELKARURABLO_H
#include "InterGame.h"
#include "Base.h"

class FelkaruRablo:public InterGame
{
public:
	FelkaruRablo();
	void toltoFelirat();
	void menufelirat(int const &a, int const &b, int const &c);
	void print(int const &a, int const &b, int const &c);
	void printKamu(int const &x, int const &y);
	void Tetrakas(Base &Jatbaz);
	void gyozelem(int const &a, int const &b, int const &c, Base &Jatbaz);
	void forog(int &a, int &b, int &c, Base &Jatbaz);
	void jatVal(Base &Jatbaz);
	void jatekStart(Base &Jatbaz);
	void valtas(Base &Jatbaz);
	void printszabalyok();
	void centerFelkaru() const;
	~FelkaruRablo();
};

#endif