#pragma once
#ifndef INTERGAME_H
#define INTERGAME_H

#include "Base.h"
#include <string>

class InterGame
{
	
public:
	std::string JatekSzabaly;
	InterGame();
	void virtual jatekStart(Base &Jatbaz) = 0;
	void virtual printszabalyok() = 0;
	virtual ~InterGame();
};

#endif