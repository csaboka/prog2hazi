#pragma once
#ifndef ZENE_H
#define ZENE_H


#include <string>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <Mmsystem.h>
#include <mciapi.h>
#pragma comment(lib, "Winmm.lib")
#include <SFML/Audio.hpp>

class Zene
{
	std::string Zenehelye;

public:
	static int ZeneHangEro;
	static float EffectHangEro;

	void zene(int hanyas) {
		switch (hanyas)
		{
		case 1: {
			Zenehelye = "Zene/Jazz.wav";
			break;
		}
		case 2: {
			Zenehelye = "Zene/Relaxing.wav";
			break;
		}
		case 3: {
			Zenehelye = "Zene/Inspiring.wav";
			break;
		}
		default: {
			std::cout << "Nem lehet lejatszani." << std::endl;
			std::cout << "Jazz fog jatszodni." << std::endl;
			system("pause");
			Zenehelye = "Zene/Jazz.wav";
			break;
		}
		}
	}

	void zeneFilebol() {
		std::string input_str;
		std::ifstream myfile;
		myfile.open("Zene/ZeneOpt.sav");
		if (myfile.is_open())
		{
			getline(myfile, Zenehelye);
			getline(myfile, input_str);
			ZeneHangEro = std::stoi(input_str);
			getline(myfile, input_str);
			EffectHangEro = std::stof(input_str);
			myfile.close();
		}
		else {
			throw std::runtime_error("Nem lehetett megnyitni a fajlt: ZeneOpt.sav");
		}
	}

	void zeneFileba() {
		std::ofstream myfile;
		myfile.open("Zene/ZeneOpt.sav");
		if (myfile.is_open())
		{
			myfile << Zenehelye << std::endl << ZeneHangEro << std::endl << EffectHangEro << std::endl;
		}
		myfile.close();
	}

	void play(sf::Music &music) {
		
		music.openFromFile(Zenehelye);
		music.setVolume((float)ZeneHangEro);
		music.play();
		music.setLoop(true);
	}

	void setZenehelye(std::string helye) {
		this->Zenehelye = helye;
	}

	std::string getZenehely() {
		return this->Zenehelye;
	}

	int getZeneHangero() {
		return this->ZeneHangEro;
	}

	float getEffectHangero() {
		return this->EffectHangEro;
	}

	void ZeneHangBeall() {
		system("cls");
		centerZene();
		unsigned szam;
		std::cout << "Zene jelenlegi hangereje: " << ZeneHangEro << std::endl;
		std::cout << "Mekkora legyen a zene hangereje?" << std::endl;
		std::cout << "(0-100)" << std::endl;
		std::cin >> szam;
		while (szam < 0 || szam > 100) {
			system("cls");
			std::cout << "Zene jelenlegi hangereje: " << ZeneHangEro << std::endl;
			std::cout << "Mekkora legyen a zene hangereje?" << std::endl;
			std::cout << "(0-100)" << std::endl;
			std::cout << "Ervenytelen szam" << std::endl;
			std::cin >> szam;
		}
		this->ZeneHangEro = szam;
	}

	void EffectHangBeall() {
		system("cls");
		centerZene();
		float szam;
		std::cout << "Effect jelenlegi hangereje: " << EffectHangEro << std::endl;
		std::cout << "Mekkora legyen a effect hangereje?" << std::endl;
		std::cout << "(0-100)" << std::endl;
		std::cin >> szam;
		while (szam < 0 || szam > 100) {
			system("cls");
			std::cout << "Effect jelenlegi hangereje: " << EffectHangEro << std::endl;
			std::cout << "Mekkora legyen a effect hangereje?" << std::endl;
			std::cout << "(0-100)" << std::endl;
			std::cout << "Ervenytelen szam" << std::endl;
			std::cin >> szam;
		}
		this->EffectHangEro = szam;
	}

	int Zenevalasztas() {
		system("cls");
		unsigned szam;
		std::cout << "Milyen fajta zene szoljon?" << std::endl;
		std::cout << "1 << Jazz" << std::endl;
		std::cout << "2 << Relax" << std::endl;
		std::cout << "3 << Inspire" << std::endl;
		std::cout << "0 << Vissza" << std::endl;
		std::cin >> szam;
		if (szam == 0) {
			return 0;
		}
		while (szam < 0 || szam > 3) {
			system("cls");
			std::cout << "Milyen fajta zene szoljon?" << std::endl;
			std::cout << "1 << Jazz" << std::endl;
			std::cout << "2 << Relax" << std::endl;
			std::cout << "3 << Inspire" << std::endl;
			std::cout << "0 << Vissza" << std::endl;
			std::cout << "Ervenytelen szam" << std::endl;
			std::cin >> szam;
			if (szam == 0) {
				return 0;
			}
		}
		zene(szam);
		return szam;
	}

	void centerZene()
	{
		std::string s = "=== |== Zene ==| ===";
		int l = (int)s.length();
		int pos = (int)((80 - l) / 2);
		for (int i = 0; i < pos; i++) {
			std::cout << " ";
		}


		std::cout << s << std::endl;
	}

	void Menufelirat() {
		centerZene();
		std::cout << " 1 >> Zene valasztas" << std::endl;
		std::cout << " 2 >> Zene hangero beallitasa" << std::endl;
		std::cout << " 3 >> Effect hangero beallitasa" << std::endl;
		std::cout << " 0 >> Vissza" << std::endl;
	}

	void zeneMenu(sf::Music &music) {

		int menuparancs;
		int jelzo = 1;

		while (jelzo == 1)
		{

			system("cls");
			Menufelirat();

			std::cout << "Kerek egy 0-3 kozotti szamot" << std::endl;
			std::cin >> menuparancs;

			while (menuparancs != 0 && menuparancs != 1 && menuparancs != 2 && menuparancs != 3 && menuparancs != 4) {
				system("cls");

				std::cout << std::endl;
				std::cout << "\t" << menuparancs << "   Nem 0-3 kozotti szam" << std::endl;
				std::cout << std::endl;
				std::cout << "Kerek egy szamot 0-3-ig" << std::endl;
				std::cin >> menuparancs;
			}

			if (menuparancs == 1) {
				int szam = Zenevalasztas();
				if (szam != 0) {
					play(music);
					zeneFileba();
				}
			}

			if (menuparancs == 2) {
				ZeneHangBeall();
				music.setVolume((float)ZeneHangEro);
				zeneFileba();
			}

			if (menuparancs == 3) {
				EffectHangBeall();
				zeneFileba();
			}

			if (menuparancs == 0) {
				jelzo = 0;
			}

			system("cls");

		}

	}
};
#endif // !ZENE_H