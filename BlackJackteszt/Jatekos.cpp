#include "Jatekos.h"
#include <iostream>
#include <string>




Jatekos::Jatekos()
{
	this->neve = "Nincs neve";
	this->sorszam = 0;
	this->kartyakszama = 0;
	this->penz = 500;
	this->jatszik = false;
	this->BlackJack = false;
	this->kartya = NULL;
	this->aktTet = 0;
	this->biztositas = 0;
}

void Jatekos::setJatszik(bool x) {
	this->jatszik = x;
} 

void Jatekos::setKartyaszama(int a) {
	this->kartyakszama = a;
}

void Jatekos::printAll() const {
	std::cout << "Jatekos " << this->sorszam << ": ";
	std::cout << this->neve << "   ";
	std::cout << this->penz << "  ";
	if (this->jatszik) { std::cout << "Jatszik  "  ; }
	else { std::cout << "Nem jatszik  "  ; }
	std::cout << this->aktTet << std::endl;
}

void Jatekos::setNeve(std::string Neve) {
	this->neve = Neve;
}

void Jatekos::setKartyaNULL() {
	this->kartya = NULL;
}

void Jatekos::setSorszam(int n) {
	this->sorszam = n;
}

void Jatekos::lapHuzas(std::string Nev, int ertek) {
	lap* tmp;
	tmp = new lap[kartyakszama + 1];
	if (kartya != NULL) {
		for (int i = 0; i < kartyakszama; i++) {
			tmp[i].setNev(kartya[i].getNev());
			tmp[i].setValue(kartya[i].getValue());
		}
	}

	tmp[kartyakszama].setNev(Nev);
	tmp[kartyakszama].setValue(ertek);
	kartya = tmp;
	kartyakszama++;
	Oszto::aktLap++;
}

void Jatekos::tet(double a) {
	this->aktTet = a;
	this->penz = this->penz - a;
}

void Jatekos::setAktTet(double a) {
	this->aktTet = a;
}

void Jatekos::setPenz(double value) {
	this-> penz = value;
}

void Jatekos::setBlackJack(bool z) {
	this->BlackJack = z;
}

void Jatekos::setBiztositas(double x)
{
	this->penz = this->penz - x;
	this->biztositas = x;
}

std::string Jatekos::getNeve() {
	return this->neve;
}

double Jatekos::getPenz() {
	return this->penz;
}

void Jatekos::print() const {
	std::cout << sorszam << ". Jatekos: " << this->neve << " kartyai:" << std::endl;
	for (int x = 0; x < kartyakszama; x++) {
		kartya[x].print();
	}
	std::cout << "Kartyak pontszama: " << getLapOsszeg() << std::endl;
	if (getLapOsszeg() > 21) {
		std::cout << "Tultelitodott" << std::endl;
	}
}

int Jatekos::getLapOsszeg() const {
	int osszeg = 0;
	for (int i = 0; i < kartyakszama; i++) {
		osszeg = osszeg + kartya[i].getValue();
	}
	return osszeg;
}

int Jatekos::getKartyakSzama()
{
	return this->kartyakszama;
}

double Jatekos::getAktTet() {
	return this->aktTet;
}

double Jatekos::getBiztositas()
{
	return this->biztositas;
}

bool Jatekos::getJatszik() {
	return this->jatszik;
}

bool Jatekos::getBlackJack() {
	return this->BlackJack;
}

Jatekos::~Jatekos()
{
}
