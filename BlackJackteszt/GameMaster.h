#pragma once
#ifndef GAMEMASTER_H
#define GAMEMASTER_H

#include "Base.h"


class GameMaster
{
	//void asztalPrint() const;
public:
	GameMaster();
	void centerCasino() const ;
	void jatekstart(Base &Jatbaz);
	void gameMenufelirat();
	~GameMaster();
};

#endif