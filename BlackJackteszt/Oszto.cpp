#include "Oszto.h"
#include "lap.h"
#include <string>
#include <iostream>


Oszto::Oszto()
{
	this->kartyakszama = 0;
	this->kartya = NULL;
}

void Oszto::lapHuzas(std::string Nev, int ertek) {
	lap* tmp;
	tmp = new lap[kartyakszama + 1];
	if (kartya != NULL) {
		for (int i = 0; i < kartyakszama; i++) {
			tmp[i].setNev(kartya[i].getNev());
			tmp[i].setValue(kartya[i].getValue());
		}
	}

	tmp[kartyakszama].setNev(Nev);
	tmp[kartyakszama].setValue(ertek);
	kartya = tmp;
	kartyakszama++;
	Oszto::aktLap++;
}

void Oszto::print() const {
	std::cout << "Oszto kartyai:" << std::endl;
	for (int x = 0; x < kartyakszama; x++) {
		kartya[x].print();
	}
	std::cout << "Kartyak pontszama: " << getLapOsszeg() << std::endl;
	if (getLapOsszeg() > 21) {
		std::cout << "Tultelitodott" << std::endl;
	}
}

int Oszto::getLapOsszeg() const {
	int osszeg = 0;
	for (int i = 0; i < kartyakszama; i++) {
		osszeg = osszeg + kartya[i].getValue();
	}
	return osszeg;
}

int Oszto::getKartyakSzama() {
	return this->kartyakszama;
}

void Oszto::printEgy() const {
	std::cout << "Oszto kartyai:" << std::endl;
	kartya[0].print();
}

Oszto::~Oszto()
{
}
